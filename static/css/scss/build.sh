#!/bin/bash

source build.conf

if [ -f "$OUTPUT" ]; then
    rm "$OUTPUT"
fi

sassc app.scss > "$OUTPUT"
