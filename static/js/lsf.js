/**
	@brief		Turn the username inputs into emails, since we only use email.
	@since		2019-04-28 23:20:51
**/
;(function( $ )
{
	$.fn.extend(
	{
		fix_login_form : function()
		{
			return this.each( function()
			{
				var $this = $(this);
				$( '#user_login', $this ).attr( 'type', 'email' );

			} ); // return this.each( function()
		} // plugin: function()
	} ); // $.fn.extend({
} )( jQuery );
;
jQuery( document ).ready( function( $ )
{
	$( '#loginform' ).fix_login_form();
	$( '#lostpasswordform' ).fix_login_form();
} );
;
