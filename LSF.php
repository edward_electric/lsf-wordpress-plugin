<?php
/*
Author:			edward_plainview
Author Email:	edward@plainviewplugins.com
Author URI:		https://plainviewplugins.com
Description:	Funktioner och finesser för LSF
Plugin Name:	Limhamns Skytteförening
Plugin URI:		https://lsf.nu/
Version:		3.002
*/

namespace lsf
{
	require_once( __DIR__ . '/vendor/autoload.php' );

	class LSF
		extends \plainview\sdk_lsf\wordpress\base
	{
		/**
			@brief		Plugin version.
			@since		2018-03-14 19:04:03
		**/
		public $plugin_version = LSF_PLUGIN_VERSION;

		use \plainview\sdk_lsf\wordpress\traits\debug;

		use menu_trait;
		use misc_methods_trait;
		use protocol1_trait;
		use plainview_lane_booking_trait;
		use tribe_events_trait;
		use user_trait;

		/**
			@brief		Constructor.
			@since		2017-12-07 19:31:43
		**/
		public function _construct()
		{
			$this->init_menu_trait();
			$this->init_misc_methods_trait();
			$this->init_plainview_lane_booking_trait();
			$this->init_protocol1_trait();
			$this->init_tribe_events_trait();
			$this->init_user_trait();
			$this->add_action( 'wp_enqueue_scripts' );
		}
	}
}

namespace
{
	define( 'LSF_PLUGIN_VERSION', 3 );

	/**
		@brief		Return the instance of ThreeWP Broadcast.
		@since		2014-10-18 14:48:37
	**/
	function LSF()
	{
		return lsf\LSF::instance();
	}

	new lsf\LSF();
}
