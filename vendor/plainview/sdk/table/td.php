<?php

namespace plainview\sdk_lsf\table;

/**
	@brief		Cell of type TD.
	@since		20130430
**/
class td
	extends cell
{
	public $tag = 'td';
}
