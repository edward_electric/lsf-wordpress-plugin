<?php

namespace lsf;

/**
	@brief		Things needed for The Events Calendar.
	@since		2019-04-06 18:17:46
**/
trait tribe_events_trait
{
	/**
		@brief		Init this trait.
		@since		2019-04-06 18:17:59
	**/
	public function init_tribe_events_trait()
	{
		$this->add_filter( 'tribe_events_event_classes', 100, 2 );
		$this->add_action( 'tribe_events_single_meta_venue_section_end' );
		define( 'TRIBE_EVENTS_INTEGRATIONS_SHOULD_LOAD_FREEMIUS', false );
	}

	/**
		@brief		The classes for this event.
		@since		2019-04-14 23:41:51
	**/
	public function tribe_events_event_classes( $classes, $event_id )
	{
		if ( ! $this->user_id() )
			return $classes;

		$state = \plainview\lane_booking\States\State::load( $event_id );
		if ( $state->participants()->all()->has( $this->user_id() ) )
			$classes [ 'pvlb_participating' ]= 'pvlb_participating';

		return $classes;
	}

	/**
		@brief          tribe_events_single_meta_venue_section_end
		@since          2018-09-13 20:10:02
	**/
	public function tribe_events_single_meta_venue_section_end()
	{
	   global $post;
	   $post_id = $post->ID;

	   $action = $this->new_action( 'tribes_lsf_section' );
	   $action->post_id = $post_id;
	   $action->text = '';
	   $action->execute();

	   if ( $this->user_id() < 1 )
	   	   return;

	   $results_pdf = get_field( 'results_pdf', $post_id );
	   if ( $results_pdf !== false )
	   {
			   $action->text .= '<dt>Resultat</dt>';
			   $action->text .= sprintf( '<dd><a href="%s">%s.pdf</a></dt>',
					   $results_pdf[ 'url' ],
					   $results_pdf[ 'title' ]
			   );
	   }

	   echo $action->text;
	}
}
