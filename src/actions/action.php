<?php

namespace lsf\actions;

/**
	@brief		Base action class.
	@since		2018-04-02 23:02:51
**/
class action
	extends \plainview\sdk_lsf\wordpress\actions\action
{
	/**
		@brief		Prefix.
		@since		2018-04-02 23:02:51
	**/
	public function get_prefix()
	{
		return 'lsf_';
	}
}
