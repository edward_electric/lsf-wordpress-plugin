<?php

namespace lsf\actions;

/**
	@brief		Allow modules to display their own stuff in the tribes info div.
	@since		2019-05-02 21:19:53
**/
class tribes_lsf_section
	extends action
{
	/**
		@brief		IN: The post ID we are working with.
		@since		2019-05-02 21:20:22
	**/
	public $post_id;

	/**
		@brief		IN/OUT: The text to display.
		@since		2019-05-02 21:20:10
	**/
	public $text;
}
