<?php

namespace lsf\actions;

/**
	@brief		Allow modules to hook into our menu.
	@since		2018-04-02 23:03:52
**/
class admin_menu
	extends action
{
	/**
		@brief		IN/OUT: The menu page to modify.
		@since		2018-04-02 23:04:03
	**/
	public $menu;
}
