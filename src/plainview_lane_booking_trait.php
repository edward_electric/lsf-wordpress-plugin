<?php

namespace lsf;

/**
	@brief		For lane bookings.
	@since		2019-04-22 23:04:42
**/
trait plainview_lane_booking_trait
{
	/**
		@brief		The _GET key of the post ID of which to generate the participants PDF.
		@since		2019-05-02 21:57:32
	**/
	public static $participants_pdf_key = 'lsf_participants_pdf';

	/**
		@brief		Init.
		@since		2019-05-02 21:22:08
	**/
	public function init_plainview_lane_booking_trait()
	{
		$this->add_action( 'lsf_tribes_lsf_section', 'pvlb_tribes_lsf_section' );
		$this->add_action( 'pvlb_participant_to_container_html' );
		$this->add_action( 'template_redirect', 'pvlb_template_redirect' );
	}

	/**
		@brief		Retrieve the nonce key for this state.
		@since		2019-05-02 21:59:16
	**/
	public function pvlb_get_participants_pdf_nonce( $post_id )
	{
		$key = 'pvlb_get_participants_pdf_nonce' . $post_id . $_SERVER[ 'REMOTE_ADDR' ];
		return $key;
	}

	/**
		@brief		Add handedness to the participant.
		@since		2019-05-04 20:26:03
	**/
	public function pvlb_participant_to_container_html( $action )
	{
		if ( ! $action->participant->get_meta( 'lsf_left_handed' ) )
			return;
		$action->container->css_class( 'lsf_left_handed' );
	}

	/**
		@brief		Maybe download the participants PDF.
		@since		2019-05-02 21:56:58
	**/
	public function pvlb_template_redirect()
	{
		if ( ! isset( $_GET[ static::$participants_pdf_key ] ) )
			return;
		$post_id = intval( $_GET[ static::$participants_pdf_key ] );
		$nonce = $_GET[ 'nonce' ];
		if ( ! wp_verify_nonce( $nonce, $this->pvlb_get_participants_pdf_nonce( $post_id ) ) )
			wp_die( 'Invalid nonce!' );

		$state = \plainview\lane_booking\States\State::load( $post_id );
		if ( ! $state->settings()->get( 'schedule_ready' ) )
			return;

		$participants_pdf = new classes\Participants_PDF();
		$participants_pdf->state = $state;
		$participants_pdf->download_pdf();
		ddd( 'create file' );
		exit;
	}

	/**
		@brief		Display PVLB related things.
		@since		2019-05-02 21:22:50
	**/
	public function pvlb_tribes_lsf_section( $action )
	{
		if ( ! $this->is_lsf_admin() )
	   	   return;

	   $state = \plainview\lane_booking\States\State::load( $action->post_id );
	   if ( ! $state->settings()->get( 'schedule_ready' ) )
	   	   return;

	   $url = get_option( 'siteurl' );
	   $url = add_query_arg( static::$participants_pdf_key, $action->post_id, $url );
	   $nonce = wp_create_nonce( $this->pvlb_get_participants_pdf_nonce( $action->post_id ) );
	   $url = add_query_arg( 'nonce', $nonce, $url );

	   $action->text .= '<dt>PDF</dt>';
	   $action->text .= sprintf( '<dd><a href="%s">Deltagarlista.pdf</a></dt>',
		   $url
	   );
	}
}
