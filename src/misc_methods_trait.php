<?php

namespace lsf;

/**
	@brief		Vraious methods that didn't fit anywhere else.
	@since		2017-12-11 14:22:41
**/
trait misc_methods_trait
{
	/**
		@brief		Remember the user forever.
		@since		2019-03-26 07:49:55
	**/
	public function auth_cookie_expiration()
	{
		return 31536000; // one year in seconds: 60 * 60 * 24 * 365
	}

	/**
		@brief		Convenience method to return a new collection.
		@since		2017-12-14 18:45:53
	**/
	public function collection( $items = [] )
	{
		return new Collection( $items );
	}

	/**
		@brief		Init the trait.
		@since		2019-04-06 18:54:15
	**/
	public function init_misc_methods_trait()
	{
		add_filter( 'widget_text', 'do_shortcode' );
		add_filter( 'send_email_change_email', '__return_false' );
		$this->add_action( 'admin_menu', 'redirect_non_admins' );
		$this->add_action( 'login_head', 'wp_enqueue_scripts' );
		$this->add_filter( 'auth_cookie_expiration' );
		$this->add_shortcode( 'lsf_internal_page' );
		$this->add_shortcode( 'lsf_login_logout_link' );
		$this->add_shortcode( 'lsf_page_content' );
		$this->add_shortcode( 'lsf_results_counter' );
		$this->add_filter( 'page_attributes_dropdown_pages_args' );
		$this->add_filter( 'quick_edit_dropdown_pages_args', 'page_attributes_dropdown_pages_args' );
		$this->add_filter( 'show_admin_bar' );
		$this->add_filter( 'gettext', 'lsf_remove_username', 20, 3 );
	}

	/**
		@brief		Is this user an LSF administrator, capable of using things like P1?
		@since		2019-05-04 18:10:54
	**/
	public function is_lsf_admin()
	{
	   return current_user_can( 'edit_posts' );
	}

	/**
		@brief		If logged in, show a link to the internal page.
		@since		2019-05-04 20:47:03
	**/
	public function lsf_internal_page()
	{
		if ( ! $this->is_lsf_admin() )
			return '';
		return '<li><a href="/internt/">Internt</a></li>';
	}

	/**
		@brief		Show a login or logout link, depending on the user.
		@since		2019-04-09 10:31:08
	**/
	public function lsf_login_logout_link()
	{
		if ( $this->user_id() > 0 )
		{
			$url = $this->current_url();
			$url = wp_logout_url( $url );
			return sprintf( '<a href="%s">Logga ut</a>', $url );
		}
		else
		{
			$url = $this->current_url();
			$url = wp_login_url( $url );
			return sprintf( '<a href="%s">Logga in</a>', $url );
		}
	}

	/**
		@brief		Remove mentions of "username" from login and password reset.
		@since		2019-04-21 11:59:07
	**/
	public function lsf_remove_username( $translated_text, $text, $domain )
	{
		switch( $text )
		{
			case 'Username or Email Address':
				return 'E-postadress';
		}
		return $translated_text;
	}

	/**
		@brief		Enclose the results and display a count.
		@since		2019-04-29 21:52:50
	**/
	public function lsf_results_counter( $atts, $content )
	{
		$count = substr_count( $content, '<a' );
		$rifle_count = ( substr_count( $content, '300m' ) / 2 ) + ( substr_count( $content, 'evär' ) / 2 );

		$r = wpautop( sprintf( 'Detta året har %d tävlingar ägt rum, varav %d pistoltävlingar.', $count, $count - $rifle_count ) );

		if ( $this->user_id() > 0 )
			$r .= $content;
		else
			$r .= wpautop( sprintf( 'Endast inloggade besökare får tillgång till resultatlistorna.' ) );

		return $r;
	}

	/**
		@brief		page_attributes_dropdown_pages_args
		@since		2019-04-06 18:54:42
	**/
	public function page_attributes_dropdown_pages_args( $args )
	{
		$args[ 'post_status' ] = [ 'publish', 'pending', 'draft', 'private' ];
		return $args;
	}

	/**
		@brief		Generate a new action.
		@details	Convenience method so that other plugins don't have to use the whole namespace for the class' actions.
		@since		2017-09-27 13:20:01
	**/
	public function new_action( $action_name )
	{
		$called_class = get_called_class();
		// Strip off the class name.
		$namespace = preg_replace( '/(.*)\\\\.*/', '\1', $called_class );
		$classname = $namespace  . '\\actions\\' . $action_name;
		return new $classname();
	}

	/**
		@brief		Site options.
		@since		2017-12-09 09:18:21
	**/
	public function local_options()
	{
		return array_merge( [
		], parent::local_options() );
	}

	/**
		@brief		Display a page which we find by its slug.
		@since		2019-04-19 22:03:19
	**/
	public function lsf_page_content( $atts = [] )
	{
		$options = array_merge( [
			'slug' => '',
		], $atts );

		$options = (object) $options;

		if ( $options->slug == '' )
			return $this->debug( 'No slug attribute given.' );

		$options->slug = sanitize_title( $options->slug );

		// We have to use a query since we are looking for posts and pages and whatever other custom post types the user might be using.
		global $wpdb;
		$query = sprintf( "SELECT `post_content` FROM `%s` WHERE `post_name` = '%s' AND `post_status` = 'publish' ORDER BY `ID` DESC LIMIT 1", $wpdb->posts, $options->slug );
		$post = $wpdb->get_row( $query );

		if ( ! $post )
			return '';

		$content = $post->post_content;
		$content = do_shortcode( $content );
		$content = wpautop( $content );

		return $content;
	}

	/**
		@brief		Redirect all non-admins from the admin.
		@since		2019-04-09 10:28:53
	**/
	public function redirect_non_admins()
	{
		if ( ! $this->is_lsf_admin() )
		{
			wp_redirect( site_url() );
			exit;
		}
	}

	/**
		@brief		Hide the admin bar from non-contributors.
		@since		2019-04-10 09:31:00
	**/
	public function show_admin_bar()
	{
		return $this->is_lsf_admin();
	}

	/**
		@brief		Return the contents of a text file using wpautop.
		@since		2018-01-28 09:39:00
	**/
	public function wpautop_file( $key )
	{
		$file = __DIR__ . '/static/texts/' . $key . '.txt';
		$text = file_get_contents( $file );
		return wpautop( $text );
	}

	/**
		@brief		Enqueue scripts.
		@since		2018-09-11 19:51:13
	**/
	public function wp_enqueue_scripts()
	{
		wp_enqueue_script( 'lsf_js', $this->paths( 'url' ) . '/static/js/lsf.js', [ 'jquery' ] );
		wp_enqueue_style( 'lsf_css', $this->paths( 'url' ) . '/static/css/lsf.css', false );
	}
}
