<?php

namespace lsf;

/**
	@brief		Setup menus.
	@since		2018-04-02 22:57:22
**/
trait menu_trait
{
	/**
		@brief		Init!
		@since		2017-12-07 19:34:05
	**/
	public function init_menu_trait()
	{
		$this->add_action( 'admin_menu' );
	}

	/**
		@brief		Admin menu callback.
		@since		2017-12-07 19:35:46
	**/
	public function admin_menu()
	{
		$this->wp_enqueue_scripts();

		$menu = $this->menu_page()
			->callback_this( 'admin_menu_tabs' )
			->capability( 'manage_options' )
			->menu_slug( 'lsf' )
			// Menu title
			->menu_title( 'LSF' )
			// Page title
			->page_title( 'Limhamns Skytteförening' )
			->icon_url( ' dashicons-admin-home' );

		$action = $this->new_action( 'admin_menu' );
		$action->menu = $menu;
		$action->execute();

		$this->menu_page()
			->add_all();
/**
		// For normal admin.
		add_submenu_page(
			'options-general.php',
			'Limhamns Skytteförening',
			'LSF',
			'manage_options',
			'lsf',
			[ $this, 'admin_menu_tabs' ]
		);
**/

	}

	public function admin_menu_tabs()
	{
		$tabs = $this->tabs();

		$tabs->tab( 'settings' )
			->callback_this( 'settings' )
			->heading( 'Inställningar' )
			->name( 'Inställningar' );

		echo $tabs->render();
	}

	/**
		@brief		Settings
		@since		2019-03-24 18:55:23
	**/
	public function settings()
	{
		$form = $this->form();
		$form->id( 'settings' );
		$r = '';

		$fs = $form->fieldset( 'fs_api_key' )
			// Fieldset label.
			->label( 'P1 API key' );

		$api_key_input = $form->text( 'api_key' )
			->description( "Nyckeln används för att kommunicera med P1 på %s", $this->p1_api()->get_url() )
			->label( 'P1 API-nyckel' )
			->readonly()
			->size( 64 )
			->value( $this->p1_api()->get_api_key() );

		$api_key_reset_input = $form->checkbox( 'api_key_reset' )
			->description( 'Skapa en helt ny nyckel som sedan måste läggas in i p1.' )
			->label( 'Skapa ny nyckel' );

		// File selector for sign in PDF
		$participants_pdf_input = $form->number( 'participants_pdf' )
			->description( 'Ange IDnummer för filen som används för att skriva ut inskrivningslistan.' )
			->label( 'PDF för inskrivning' )
			->value( $this->get_option( 'participants_pdf' ) );

		$form->primary_button( 'save' )
			->value( __( 'Save settings', 'pvlb' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();

			if ( $api_key_reset_input->is_checked() )
				$this->p1_api()->generate_api_key();

			$this->update_option( 'participants_pdf', $participants_pdf_input->get_filtered_post_value() );

			$r = $this->info_message_box()->_( __( 'Settings saved!', 'pvlb' ) );

			echo $r;

			$_POST = [];
			$function = __FUNCTION__;
			echo $this->$function();
			return;
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		echo $r;
	}
}

/**
		if ( isset( $_GET[ 'fix_end_dates' ] ) )
		{
			global $wpdb;
			foreach( [
				'_EventStartDate' => '_EventEndDate',
				'_EventStartDateUTC' => '_EventEndDateUTC',
			] as $source_column => $target_column )
			{
				$query = sprintf( "SELECT * FROM `%s` WHERE `meta_key` = '%s'", $wpdb->postmeta, $source_column );
				$results = $wpdb->get_results( $query );
				foreach( $results as $result )
				{
					$query = sprintf( "UPDATE `%s` SET `meta_value` = '%s' WHERE `post_id` = '%s' AND `meta_key` = '%s'",
						$wpdb->postmeta,
						$result->meta_value,
						$result->post_id,
						$target_column
					);
					ddd( $query );
					$wpdb->get_results( $query );
				}
			}
		}
**/
