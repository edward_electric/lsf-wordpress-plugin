<?php

namespace lsf\classes;

use \plainview\lane_booking\States\State;

/**
	@brief		The PDF handler for creating participant lists.
	@since		2019-05-02 22:12:21
**/
class Participants_PDF
{
	/**
		@brief		The state we are working with.
		@since		2019-05-02 22:12:48
	**/
	public $state;

	/**
		@brief		Download the PDF.
		@since		2019-05-02 22:13:09
	**/
	public function download_pdf()
	{
		$template_id = LSF()->get_option( 'participants_pdf' );
		$filename = get_attached_file( $template_id );

		if ( ! is_readable( $filename ) )
			wp_die( sprintf( 'Could not read file %s', $filename ) );

		$post = get_post( $this->state->post_id );
		$pdf_filename = sprintf( 'Deltagarlista %s %s.pdf',
			$post->post_title,
			date( 'Y-m-d H:i', $this->state->get_time_offset( $this->state->get_event_time() ) )
		);

		// Store the template index data for mpdf for each page.
		$template_pages = [];

		$mpdf = new \Mpdf\Mpdf( [
			'orientation' => 'L',
			'tempDir' => sys_get_temp_dir(),
		] );

		// Load the template
		$pages = $mpdf->setSourceFile( $filename );
		for( $counter = 1; $counter <= $pages; $counter++ )
			$template_pages[ $counter ] = $mpdf->importPage( $counter );

		// Output the participants.
		foreach( $this->state->groups() as $group_id => $group )
		{
			$halves = 0;
			foreach( $group->active() as $lane_id => $user_id )
			{
				if ( $halves < 1 )
				{
					$mpdf->AddPage();
					$mpdf->useTemplate( $template_pages[ 1 ] );
					$halves++;

					// The first thing to do here is to write all the passives.
					$passives = [];
					foreach( $group->passive() as $passive_user_id )
					{
						$participant = $this->state->participants()->all()->get( $passive_user_id );
						$passives []= $participant->get( 'name' );
					}
					$passives = implode( ', ', $passives );
					$mpdf->WriteFixedPosHTML( $passives, 22, 171, 100 , 20 );

					// Comp title
					$mpdf->WriteFixedPosHTML( $pdf_filename, 180, 171, 100 , 20 );
					// Group
					$mpdf->WriteFixedPosHTML( 'Grupp ' . $group_id, 180, 175, 100 , 20 );
				}
				if ( $halves < 2 )
					if ( $lane_id > 12 )
					{
						$mpdf->AddPage();
						$mpdf->useTemplate( $template_pages[ 2 ] );
						$halves++;

						// Comp title
						$mpdf->WriteFixedPosHTML( $pdf_filename, 180, 171, 100 , 20 );
						// Group
						$mpdf->WriteFixedPosHTML( 'Grupp ' . $group_id, 180, 175, 100 , 20 );
					}
				$participant = $this->state->participants()->all()->get( $user_id );
				$relative_lane = $lane_id;
				if ( $halves > 1 )
					$relative_lane -= 12;
				// Name.
				$y = $relative_lane * 12 + 15;
				$x = 32;
				$mpdf->WriteFixedPosHTML( '<span style="font-size: 1.5em;">' . $participant->get( 'name' ) . '</span>', $x, $y, 100 , 20 );
				// Gun cat.
				$x = 109.5;
				$mpdf->WriteFixedPosHTML( '<span style="font-size: 1.5em;">' . $participant->get( 'gun_class' ) . '</span>', $x, $y, 100 , 20 );
			}
		}

		ddd( $pdf_filename );

		$mpdf->output( $pdf_filename, \Mpdf\Output\Destination::INLINE );
		exit;
	}
}