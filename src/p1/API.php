<?php

namespace lsf\p1;

/**
	@brief		The API for communicating with the API.
	@since		2019-04-27 15:00:48
**/
class API
{
	/**
		@brief		The API key for communication with p1.
		@since		2019-04-26 10:34:35
	**/
	public static $p1_api_key = 'p1_api_key';

	/**
		@brief		Apply a bunch of settings depending on the event type.
		@since		2019-04-28 12:26:21
	**/
	public function apply_settings( $post_id )
	{
		require_once( __DIR__ . '/quicksettings.php' );

		$data = [];

		$post = get_post( $post_id );
		$terms = wp_get_object_terms( $post_id, 'tribe_events_cat' );

		$practice = false;

		foreach( $terms as $term )
		{
			$term_name = $term->name;

			if ( $term_name == 'Pistolträning' )
				$practice = true;

			if ( isset( $quicksettings[ $term_name ] ) )
			{
				$settings = $quicksettings[ $term_name ];
				$data = array_merge( $data, $settings );
			}
		}

		// Put the participants in the competition.
		$state = \plainview\lane_booking\States\State::load( $post_id );
		$preloaded_participants = [];

		// Add all officials.
		foreach( $state->groups() as $group_id => $ignore )
			foreach( $state->groups()->group( $group_id )->passive() as $participant_id )
			{
				// Do not add this participant if he is active sometime.
				if ( $state->participants()->is_active( $participant_id ) )
					continue;

				// Create the group if it doesn't already exist.
				if ( ! isset( $preloaded_participants[ $group_id ] ) )
					$preloaded_participants[ $group_id ] = [];

				$participant = $state->participants()->all()->get( $participant_id );
				$preloaded_participants[ 1 ] []= [
					'name' => $participant->get( 'name' ),
					'lane' => 'F',
					'participant_class' => 'F',
					'weapon_group' => 'C',
				];
			}

		foreach( $state->groups() as $group_id => $ignore )
			foreach( $state->groups()->group( $group_id )->active() as $lane_id => $participant_id )
			{
				if ( ! isset( $preloaded_participants[ $group_id ] ) )
					$preloaded_participants[ $group_id ] = [];

				// The license status is stored in the user meta.
				$key = 'lsf_license';
				$lsf_license = get_user_meta( $participant_id, $key, true );

				$participant = $state->participants()->all()->get( $participant_id );
				if ( ! $participant )
					continue;
				$preloaded_participants[ $group_id ] []= [
					'name' => $participant->get( 'name' ),
					'participant_class' => ( $lsf_license ? 'ML' : 'UL' ),
					'personal_handicap' => intval( get_user_meta( $participant_id, 'lsf_personal_handicap', true ) ),
					'weapon_group' => $participant->get_gun_class(),
					'lane' => ( $practice ? 'T' : $lane_id ),
				];
			}

		if ( count( $preloaded_participants ) > 0 )
			$data[ 'preloaded_participants' ] = $preloaded_participants;

		$data[ 'group' ] = 1;

		$EventStartDate = strtotime( get_post_meta( $post->ID, '_EventStartDate', true ) );
		if ( $EventStartDate > 0 )
			$data[ 'info' ][ 'date' ] = date( 'Y-m-d', $EventStartDate );

		$competition_code = $this->get_competition_code( $post_id );
		$this->set_state( $competition_code, $data );
	}

	/**
		@brief		Return the state of a competition.
		@since		2019-04-27 21:26:32
	**/
	public function fetch_competition_state( $competition_id )
	{
		// First fetch the normal url, in order to perhaps create the competition. Shouldn't be necessary if the comp is already created.
		$url = sprintf( '%s/competition/%s',
			static::get_api_url(),
			$competition_id
		);
		$response = wp_remote_get( $url );
		// NOW we fetch the state.
		$url = sprintf( '%s/competition/%s/get_state',
			static::get_api_url(),
			$competition_id
		);
		$response = wp_remote_get( $url );
		$body = wp_remote_retrieve_body( $response );
		$json = json_decode( $body );
		return $json;
	}

	/**
		@brief		Fetch the results PDF and upload it.
		@since		2019-04-27 19:07:47
	**/
	public function fetch_pdf( $post_id )
	{
		$cc = $this->get_competition_code( $post_id );
		$state = $this->fetch_competition_state( $cc );
		$pdf_url = $state->urls->export_results_pdf;

		$response = wp_remote_get( $pdf_url );

		$filename = $response[ 'headers' ][ 'content-disposition' ];
		$filename = explode( '"', $filename );
		$filename = $filename[ 1 ];

		// Find the results page.
		$results_page = get_posts( [
			'title' => 'Tävlingsresultat ' . date( 'Y' ),
			'post_type' => 'page',
		] );
		if ( count( $results_page ) < 1 )
			throw new Exception( 'Unable to find results page' );

		$results_page = reset( $results_page );

		$upload_dir = wp_upload_dir();
		$target = $upload_dir[ 'path' ] . '/' . $filename;
		file_put_contents( $target, wp_remote_retrieve_body( $response ) );

		// Upload the file.
		$just_filename = str_replace( '.pdf', '', $filename );	// Basename doesn't work.
		$wp_filetype = wp_check_filetype( $target, null );
		$attachment = [
			'post_mime_type' => $wp_filetype[ 'type' ],
			'post_name' => $just_filename,
			'post_title' => $just_filename,
		];
		$new_attachment_id = wp_insert_attachment( $attachment, $target, $results_page->ID );

		// Add the new link at the top.
		$html = sprintf( '<a href="%s">%s</a>', wp_get_attachment_url( $new_attachment_id ), basename( $filename ) ) . "\n";

		// Replace only from the first link, allowing custom text to appear before the link.
		$results_page->post_content = preg_replace( '/<a/', $html . '<a', $results_page->post_content, 1 );

		wp_update_post( [
			'ID' => $results_page->ID,
			'post_content' => $results_page->post_content,
		] );

		// And now set the results PDF custom field.
		update_post_meta( $post_id, 'results_pdf', $new_attachment_id );
	}

	/**
		@brief		Find the post ID that has this competition code, if any.
		@since		2019-04-28 22:03:30
	**/
	public function find_post_by_competition_code( $competition_code )
	{
		global $wpdb;
		$query = sprintf( "SELECT `ID` FROM `%s` WHERE `ID` IN ( SELECT `post_id` FROM `%s` WHERE `meta_key` = 'p1_code' AND `meta_value` = '%s' ) AND `post_type` = 'tribe_events' AND `post_status` = 'publish'",
			$wpdb->posts,
			$wpdb->postmeta,
			$competition_code
		);
		$results = $wpdb->get_var( $query );
		return $results;
	}

	/**
		@brief		Generate an API key.
		@since		2019-04-26 10:32:11
	**/
	public static function generate_api_key()
	{
		$key = md5( microtime() );
		$key = substr( $key, 0, 8 );	// 8 chars is enough.

		update_option( static::$p1_api_key, $key );
	}

	/**
		@brief		Return the API key for this site.
		@since		2019-04-26 10:30:16
	**/
	public static function get_api_key()
	{
		$key = get_option( static::$p1_api_key );
		if ( ! $key )
			$key = static::generate_api_key();

		$json = (object)[];
		$json->url = get_option( 'siteurl' );
		$json->url = add_query_arg( static::$p1_api_key, $key, $json->url );

		$json = json_encode( $json );
		$r = base64_encode( $json );
		$r = str_replace( '/', '_', $r );		// Replace the forward slashes with underscores because otherwise it confuses the route.
		return $r;
	}

	/**
		@brief		Generate a P1 code for this event.
		@since		2019-04-14 22:45:40
	**/
	public function generate_competition_code( $post_id )
	{
		$code = '';
		for( $counter = 0; $counter < 2; $counter++ )
		{
			$characters = 'bcdfghjkmnpqrstvw';
			$code .= substr( str_shuffle( $characters ), 0, 1 );
			$characters = 'aeou';
			$code .= substr( str_shuffle( $characters ), 0, 1 );
		}
		if ( rand( 0, 100 ) > 50 )
			$code .= rand( 10, 99 );

		// Save it to the post meta.
		update_post_meta( $post_id, 'p1_code', $code );

		return $code;
	}

	/**
		@brief		Fetch the P1 code for this post.
		@since		2019-04-27 19:10:58
	**/
	public function get_competition_code( $post_id )
	{
		$competition_code = get_post_meta( $post_id, 'p1_code', true );
		if ( ! $competition_code )
			$competition_code = $this->generate_competition_code( $post_id );
		return $competition_code;
	}

	/**
		@brief		Return the P1 URL.
		@since		2019-04-28 10:04:59
	**/
	public static function get_api_url()
	{
		$url = static::get_url();
		$url .= 'api/%s';
		$url = sprintf( $url, static::get_api_key() );

		return $url;
	}

	/**
		@brief		Return the base P1 URL.
		@since		2019-04-28 21:52:59
	**/
	public static function get_url()
	{
		if ( defined( 'P1_URL' ) )
			$url = P1_URL;
		else
			$url = 'https://p1.lsf.nu/';
		return $url;
	}

	/**
		@brief		Handle the request.
		@since		2019-04-27 15:02:11
	**/
	public function handle_request( $request )
	{
		if ( ! isset( $_GET[ 'action' ] ) )
			return;
		if ( ! isset( $_GET[ static::$p1_api_key ] ) )
			return;
		$key = get_option( static::$p1_api_key );
		if ( $_GET[ static::$p1_api_key ] != $key )
			return;
		switch( $_GET[ 'action' ] )
		{
			case 'maybe_fetch_pdf':
				$competition_code = $_GET[ 'code' ];
				// Find the post with this code.
				$post_id = $this->find_post_by_competition_code( $competition_code );
				if ( ! $post_id )
					break;
				$this->maybe_fetch_pdf( $post_id );
				echo json_encode( [
					'result' => 'ok',
					'message' => 'Inlägg ' . $post_id,
				] );
				exit;
			break;
		}
	}

	/**
		@brief		If the post does not have a pdf set, try to fetch and set it.
		@since		2019-04-27 23:08:58
	**/
	public function maybe_fetch_pdf( $post_id )
	{
		$id = get_post_meta( $post_id, 'results_pdf', true );
		if ( $id > 0 )
		{
			$attachment = get_post( $id );
			if ( $attachment !== null )
				return;
		}

		$this->fetch_pdf( $post_id );
	}

	/**
		@brief		Set the state for this competition.
		@since		2019-04-28 10:04:30
	**/
	public function set_state( $competition_id, $data )
	{
		$url = static::get_api_url();

		$url = sprintf( '%s/competition/%s/set_state',
			$url,
			$competition_id
		);
		$r = wp_remote_post( $url, [
			'body' => $data,
		] );
	}
}
