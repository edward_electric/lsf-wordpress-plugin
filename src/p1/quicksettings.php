<?php

$quicksettings = [
	'A mot B mot C' => [
		'info' => [
			'name' => 'A mot B mot C',
			'section' => 'Pistol',
		],
		'competition_handicaps' => [
			'A' => '3',
			'B' => '1',
			'C' => '0',
		],
		'competition_series' => [
			0 => [ 'type' => 'precision', 'shots' => 5, ],
			1 => [ 'type' => 'precision', 'shots' => 5, ],
			2 => [ 'type' => 'precision', 'shots' => 5, ],
			3 => [ 'type' => 'precision', 'shots' => 5, ],
			4 => [ 'type' => 'precision', 'shots' => 5, ],
			5 => [ 'type' => 'precision', 'shots' => 5, ],
		],
	],
	'Gevärstävling' => [
		'info' => [
			'name' => '300m',
			'section' => 'Gevär',
		],
		'competition_classes' => [
			'Diopter' => 'Diopter',
			'Kikarsikte' => 'Kikarsikte',
			'Funktionärer' => 'F',
			'Träning' => 'T',
		],
		'competition_prices' => [
			'Eget gevär 50kr' => 50,
			'Klubbgevär gevär 250kr' => 250,
			'Vapendragare 0kr' => 0,
		],
		'competition_series' => [
			0 => [ 'type' => 'precision', 'shots' => 5, ],
			1 => [ 'type' => 'precision', 'shots' => 5, ],
			2 => [ 'type' => 'precision', 'shots' => 5, ],
		],
	],
	'Halv Helmatch' => [
		'info' => [
			'name' => 'Halv Helmatch',
			'section' => 'Pistol',
		],
		'competition_series' => [
			0 => [ 'type' => 'precision', 'shots' => 5, ],
			1 => [ 'type' => 'precision', 'shots' => 5, ],
			2 => [ 'type' => 'precision', 'shots' => 5, ],
			3 => [ 'type' => 'precision', 'shots' => 5, ],
			4 => [ 'type' => 'precision', 'shots' => 5,	'next_lane' => 0, ],
			5 => [ 'type' => 'precision', 'shots' => 5,	'next_lane' => 0, ],
			6 => [ 'type' => 'precision', 'shots' => 5,	'next_lane' => 0, ],
			7 => [ 'type' => 'precision', 'shots' => 5, ],
		],
	],
	'Månadstävling' => [
		'info' => [
			'name' => 'Månadstävling',
			'section' => 'Pistol',
		],
		'competition_series' => [
			0 => [ 'type' => 'precision', 'shots' => 5, ],
			1 => [ 'type' => 'precision', 'shots' => 5, ],
			2 => [ 'type' => 'precision', 'shots' => 5, ],
			3 => [ 'type' => 'precision', 'shots' => 5, ],
			4 => [ 'type' => 'precision', 'shots' => 5, ],
			5 => [ 'type' => 'precision', 'shots' => 5, ],
		],
	],
	'Poängfält' => [
		'info' => [
			'name' => 'Poängfält',
			'section' => 'Pistol',
		],
		'competition_series' => [
			0 => [ 'type' => 'precision', 'shots' => 6,	'next_lane' => 0, ],
			1 => [ 'type' => 'precision', 'shots' => 6,	'next_lane' => 0, ],
			2 => [ 'type' => 'precision', 'shots' => 6,	'next_lane' => 0, ],
			3 => [ 'type' => 'precision', 'shots' => 6, ],
			4 => [ 'type' => 'precision', 'shots' => 6,	'next_lane' => 0, ],
			5 => [ 'type' => 'precision', 'shots' => 6,	'next_lane' => 0, ],
			6 => [ 'type' => 'precision', 'shots' => 6,	'next_lane' => 0, ],
			7 => [ 'type' => 'precision', 'shots' => 6, ],
		],
	],
	'Standardpistol' => [
		'info' => [
			'name' => 'Standardpistol',
			'section' => 'Pistol',
		],
		'competition_series' => [
			0 => [ 'type' => 'precision', 'shots' => 5, 'time' => 150, ],
			1 => [ 'type' => 'precision', 'shots' => 5, 'time' => 150, ],
			2 => [ 'type' => 'precision', 'shots' => 5, 'time' => 150, ],
			3 => [ 'type' => 'precision', 'shots' => 5, 'time' => 150, ],
			4 => [ 'type' => 'precision', 'shots' => 5, 'time' => 20, ],
			5 => [ 'type' => 'precision', 'shots' => 5, 'time' => 20, ],
			6 => [ 'type' => 'precision', 'shots' => 5, 'time' => 20, ],
			7 => [ 'type' => 'precision', 'shots' => 5, 'time' => 20, ],
			8 => [ 'type' => 'precision', 'shots' => 5, 'time' => 10, ],
			9 => [ 'type' => 'precision', 'shots' => 5, 'time' => 10, ],
			10 => [ 'type' => 'precision', 'shots' => 5, 'time' => 10, ],
			11 => [ 'type' => 'precision', 'shots' => 5, 'time' => 10, ],
		],
	],
	'Pistolträning' => [
		'info' => [
			'name' => 'Träning',
			'section' => 'Pistol',
		],
		'competition_series' => [],
	],
	'Veteran special' => [
		'info' => [
			'name' => 'Veteran special',
			'section' => 'Pistol',
		],
		'personal_handicaps' => true,
		'competition_series' => [
			0 => [ 'type' => 'precision', 'shots' => 5, ],
			1 => [ 'type' => 'precision', 'shots' => 5, ],
			2 => [ 'type' => 'precision', 'shots' => 5, ],
			3 => [ 'type' => 'precision', 'shots' => 5, ],
			4 => [ 'type' => 'precision', 'shots' => 5, ],
			5 => [ 'type' => 'precision', 'shots' => 5, ],
		],
	],
];

$fm = 'Föreningsmästerskap';
$quicksettings[ $fm  ] = $quicksettings[ 'Månadstävling' ];
$quicksettings[ $fm ][ 'info' ][ 'name' ] = $fm;
