<?php

namespace lsf;

/**
	@brief		Things related to users.
	@since		2019-05-04 17:52:48
**/
trait user_trait
{
	/**
		@brief		Init this trait.
		@since		2019-05-04 17:52:48
	**/
	public function init_user_trait()
	{
		$this->add_action( 'edit_user_profile' );
		$this->add_action( 'edit_user_profile_update' );
		$this->add_filter( 'manage_users_columns', 'user_manage_users_columns' );
		$this->add_filter( 'manage_users_custom_column', 'user_manage_users_custom_column', 10, 3 );

		remove_all_filters( 'enable_edit_any_user_configuration' );
		add_filter( 'enable_edit_any_user_configuration', '__return_true');

		$this->add_filter( 'map_meta_cap', 1, 4 );
		//$this->add_action( 'admin_head', 'user_admin_head' );
	}

	/**
		@brief		Add LSF inputs to the user's profile.
		@since		2019-05-04 18:04:18
	**/
	public function edit_user_profile( $user )
	{
		if ( ! $this->is_lsf_admin() )
			return;
		echo '<h2>LSF</h2>';

		$form = $this->get_user_form( $user );
		echo $form->display_form_table();
	}

	/**
		@brief		edit_user_profile_update
		@since		2019-05-04 18:15:46
	**/
	public function edit_user_profile_update( $user_id )
	{
		if ( ! $this->is_lsf_admin() )
			return;
		$user = get_user_by( 'id', $user_id );
		$form = $this->get_user_form( $user );
		$form->post();
		$form->use_post_values();

		$key = 'lsf_left_handed';
		if ( $form->input( $key )->is_checked() )
			update_user_meta( $user_id, $key, true );
		else
			delete_user_meta( $user_id, $key );

		$key = 'lsf_license';
		if ( $form->input( $key )->is_checked() )
			update_user_meta( $user_id, $key, true );
		else
			delete_user_meta( $user_id, $key );

		$key = 'lsf_personal_handicap';
		$value = $form->input( $key )->get_filtered_post_value();
		if ( $value < 1 )
			delete_user_meta( $user_id, $key);
		else
			update_user_meta( $user_id, $key, $value );
	}

	/**
		@brief		Retrieves the form for editing the user.
		@since		2019-05-04 18:06:15
	**/
	public function get_user_form( $user )
	{
		$form = $this->form();

		$key = 'lsf_license';
		$form->checkbox( $key )
			->checked( get_user_meta( $user->ID, $key, true ) )
			->description( 'Ange ifall personen har någon form av enhandslicens.' )
			->label( 'Licens' );

		$key = 'lsf_left_handed';
		$form->checkbox( $key )
			->checked( get_user_meta( $user->ID, $key, true ) )
			->description( 'Ange ifall personen alltid behöver låna vänstervapen.' )
			->label( 'Vänsterhänt' );

		$key = 'lsf_personal_handicap';
		$form->number( $key )
			->description( 'Handikappen används på Veteran Special-tävlingar. Talet anger pluspoäng per serie. 55 år ger +1, 65 år +2.' )
			->label( 'Personlig handikapp' )
			->min( 0, 2 )
			->value( intval( get_user_meta( $user->ID, $key, true ) ) );

		return $form;
	}

	function map_meta_cap( $caps, $cap, $user_id, $args )
	{
		foreach( $caps as $key => $capability )
		{
			if( $capability != 'do_not_allow' )
				continue;

			switch( $cap )
			{
				case 'edit_user':
				case 'edit_users':
					$caps[$key] = 'edit_users';
				break;
				case 'delete_user':
				case 'delete_users':
					$caps[$key] = 'delete_users';
					break;
				case 'create_users':
					$caps[$key] = $cap;
					break;
			}
		}
		return $caps;
	}

	/**
		* Checks that both the editing user and the user being edited are
		* members of the blog and prevents the super admin being edited.
	*/
 	function user_admin_head()
	{
		global $profileuser;

		$screen = get_current_screen();

		$current_user = wp_get_current_user();

		if( ! is_super_admin( $current_user->ID ) && in_array( $screen->base, array( 'user-edit', 'user-edit-network' ) ) ) { // editing a user profile
			if ( is_super_admin( $profileuser->ID ) ) { // trying to edit a superadmin while less than a superadmin
				wp_die( __( 'You do not have permission to edit this user.' ) );
			} elseif ( ! ( is_user_member_of_blog( $profileuser->ID, get_current_blog_id() ) && is_user_member_of_blog( $current_user->ID, get_current_blog_id() ) )) { // editing user and edited user aren't members of the same blog
				wp_die( __( 'You do not have permission to edit this user.' ) );
			}
		}

	}

	/**
		@brief		Hide some stupid columns.
		@details	We'll put the data into the LSF column.
		@since		2019-05-05 11:48:13
	**/
	public function user_manage_users_columns( $columns )
	{
		unset( $columns[ 'name' ] );
		unset( $columns[ 'posts' ] );
		$columns[ 'lsf' ] = 'LSF';
		return $columns;
	}

	/**
		@brief		Modify the column data to show LSF info.
		@since		2019-05-05 11:43:01
	**/
	public function user_manage_users_custom_column( $val, $column_name, $user_id )
	{
		$r = [];
		switch( $column_name )
		{
			case 'lsf':
				$user = get_user_by( 'id', $user_id );
				$r[] = $user->data->display_name;

				$key = 'lsf_license';
				$value = get_user_meta( $user_id, $key, true );
				if ( ! $value )
					$r []= '<del>Licens</del>';

				$key = 'lsf_left_handed';
				$value = get_user_meta( $user_id, $key, true );
				if ( $value )
					$r []= 'Vänsterhänt';

				$key = 'lsf_personal_handicap';
				$value = get_user_meta( $user_id, $key, true );
				if ( $value > 0 )
					$r []= '+' . $value;

			break;
		}
		$r = implode( "\n", $r );
		$r = wpautop( $r );

		return $r;
	}
}
