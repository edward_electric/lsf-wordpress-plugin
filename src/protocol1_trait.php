<?php

namespace lsf;

/**
	@brief		Handle communication with P1.
	@since		2019-04-22 23:06:57
**/
trait protocol1_trait
{
	/**
		@brief		Init.
		@since		2019-04-22 23:07:20
	**/
	public function init_protocol1_trait()
	{
		$this->add_action( 'pvlb_prepare_post_editor_form' );
		$this->add_action( 'pvlb_process_post_editor_form' );
		$this->add_action( 'template_redirect', 'p1_template_redirect' );
		$this->add_action( 'lsf_tribes_lsf_section', 'p1_tribes_lsf_section' );
	}

	/**
		@brief		Return an instance of the P1 API.
		@since		2019-04-27 14:59:22
	**/
	public function p1_api()
	{
		if ( ! isset( $this->__p1_api ) )
			$this->__p1_api = new p1\API();
		return $this->__p1_api;
	}

	/**
		@brief		Show some text on the overview.
		@since		2019-04-26 10:29:02
	**/
	public function p1_dashboard_overview()
	{
		$form = $this->form();

		$r = '';

		$key = $this->p1_api()->get_api_key();
		$form->text( 'p1_api_key' )
			->description( 'API-nyckeln som matas in i P1 inställningarna så att kommunikation kan ske.' )
			->label( 'P1 API-nyckel' )
			->size( 64 )
			->value( $key );

		$r .= $form->display_form_table();

		return $r;
	}

	/**
		@brief		p1_template_redirect
		@since		2019-04-22 23:08:02
	**/
	public function p1_template_redirect()
	{
		if ( ! isset( $_GET[ p1\API::$p1_api_key ] ) )
			return;
		$this->p1_api()->handle_request( $_POST );
		return;
	}

	/**
		@brief		p1_tribes_lsf_section
		@since		2019-05-02 21:18:28
	**/
	public function p1_tribes_lsf_section( $action )
	{
		if ( ! $this->is_lsf_admin() )
			return;

	   $state = \plainview\lane_booking\States\State::load( $action->post_id );
	   if ( ! $state->settings()->is_enabled() )
	   	   return;

	   // Does this have a p1 code?
	   $p1_code = $this->p1_api()->get_competition_code( $action->post_id );
	   $url = sprintf( "%scompetition/%s",
		   $this->p1_api()->get_url(),
		   $p1_code );
	   $action->text .= '<dt>P1</dt>';
	   $action->text .= sprintf( '<dd><a href="%s">%s</a></dt>',
		   $url,
		   $p1_code
	   );
	}

	/**
		@brief		Attach things to the form.
		@since		2019-04-27 18:58:15
	**/
	public function pvlb_prepare_post_editor_form( $action )
	{
		$action->form->p1_fetch_pdf = $action->form->checkbox( 'p1_fetch_pdf' )
			->description( 'Hämta resultatPDFen från tävlingen och lägger upp den på hemsidan.' )
			->label( 'Hämta PDF från tävlingen' );
	}

	/**
		@brief		pvlb_process_post_editor_form
		@since		2019-04-27 19:04:43
	**/
	public function pvlb_process_post_editor_form( $action )
	{
		if ( ! isset( $action->form->p1_fetch_pdf ) )
			return;

		$post = get_post( $action->post_id );

		$this->p1_api()->apply_settings( $action->post_id );

		if ( $action->form->p1_fetch_pdf->is_checked() )
			$this->p1_api()->maybe_fetch_pdf( $action->post_id );
	}
}
